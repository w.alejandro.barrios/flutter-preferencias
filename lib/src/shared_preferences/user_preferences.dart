import 'package:preferencias_usuario_app/src/pages/home_page.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserPreferences {

// Implementando Singleton
  static final UserPreferences _instance = new UserPreferences._();

  factory UserPreferences() { // Retorna una instancia existente sin tener que crear una nueva -> Si existiera
    return _instance;
  }

  UserPreferences._();

  SharedPreferences _preferences;

  initPreferences() async {
    _preferences = await SharedPreferences.getInstance();
  }

  // Getters y Setters
  get genero {
    return _preferences.getInt('genero') ?? 1; // ?? Si es null
  }

  set genero(int value) {
    _preferences.setInt('genero', value);
  }

  get colorSecundario {
    return _preferences.getBool('colorSecundario') ?? false; // ?? Si es null
  }

  set colorSecundario(bool value) {
    _preferences.setBool('colorSecundario', value);
  }

  get nombre {
    return _preferences.getString('nombre') ?? ''; // ?? Si es null
  }

  set nombre(String value) {
    _preferences.setString('nombre', value);
  }

  get ultimaPagina {
    return _preferences.getString('ultimaPagina') ?? HomePage.routeName; // ?? Si es null
  }

  set ultimaPagina(String value) {
    _preferences.setString('ultimaPagina', value);
  }

}