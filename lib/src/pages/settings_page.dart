import 'package:flutter/material.dart';
import 'package:preferencias_usuario_app/src/shared_preferences/user_preferences.dart';
import 'package:preferencias_usuario_app/src/widgets/menu_widget.dart';

class SettingsPage extends StatefulWidget {
  static final String routeName = 'settings';

  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  bool _colorSecundario;
  int _genero;
  String _nombre;
  TextEditingController _textController;
  final preferences = new UserPreferences();

  @override
  void initState() {
    super.initState();
    _genero = preferences.genero;
    _colorSecundario = preferences.colorSecundario;
    _nombre = preferences.nombre;
    preferences.ultimaPagina = SettingsPage.routeName;
    _textController = new TextEditingController(text: _nombre);
  }

  _setSelectedRadio(int value) {

    _genero = value;
    preferences.genero = value;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Ajustes'),
        backgroundColor: preferences.colorSecundario ? Colors.teal : Colors.blue,
      ),
      drawer: MenuWidget(),
      body: ListView(
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(10.0),
            child: Text('Settings',
                style: TextStyle(fontSize: 45.0, fontWeight: FontWeight.bold)),
          ),
          Divider(),
          SwitchListTile(
              value: _colorSecundario,
              title: Text('Color Secundario'),
              onChanged: (value) {
                setState(() {
                  _colorSecundario = value;
                  preferences.colorSecundario = _colorSecundario;
                });
              }),
          RadioListTile(
              value: 1,
              groupValue: _genero,
              title: Text('Masculino'),
              onChanged: _setSelectedRadio),
          RadioListTile(
              value: 2,
              groupValue: _genero,
              title: Text('Femenino'),
              onChanged: _setSelectedRadio),
          Divider(),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 20.0),
            child: TextField(
              controller: _textController,
              decoration: InputDecoration(
                  labelText: 'Nombre',
                  helperText: 'Nombre de la persona usando el teléfono'),
              onChanged: (value) {
                preferences.nombre = value;
              },
            ),
          )
        ],
      ),
    );
  }
}
