import 'package:flutter/material.dart';
import 'package:preferencias_usuario_app/src/shared_preferences/user_preferences.dart';
import 'package:preferencias_usuario_app/src/widgets/menu_widget.dart';

class HomePage extends StatelessWidget {

  static final String routeName = 'home';
  final preferences = new UserPreferences();

  @override
  Widget build(BuildContext context) {

    preferences.ultimaPagina = routeName;

    return Scaffold(
      appBar: AppBar(
        title: Text('Preferencias de Usuario'),
        backgroundColor: preferences.colorSecundario ? Colors.teal : Colors.blue,
      ),
      drawer: MenuWidget(),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text('Color Segundario: ${preferences.colorSecundario}'),
          Divider(),
          Text('Género: ${preferences.genero == 1 ? "Masculino" : "Femenino"}'),
          Divider(),
          Text('Nombre usuario: ${preferences.nombre}'),
          Divider()
        ],
      ),
    );
  }

}